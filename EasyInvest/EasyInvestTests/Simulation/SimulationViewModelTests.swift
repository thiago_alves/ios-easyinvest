//
//  SimulationViewModelTests.swift
//  EasyInvestTests
//
//  Created by Thiago Felipe Alves on 31/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import XCTest

@testable import EasyInvest

class SimulationViewModelTests: XCTestCase {

    private let viewModel = InvestViewModel(with: InvestModelFactory.generate())

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSimulateSuccesfully () {

        let expectedResult = SimulatorResultModelFactory.generate()
        let invest = InvestModelFactory.generate()

        let mockApi = InvestApiMock(withEnvironment: .production)
        mockApi.simulateInvest(amount: invest.investAmount!,
                               rate: invest.cdiPercent!,
                               date: invest.finalDate!,
                               success: { (result) in

            XCTAssertNotNil(result)
            XCTAssertEqual(result, expectedResult, "result is equal to expected")

        }, failure: { (fail) in
            XCTFail("\(fail)")
        })
    }

}
