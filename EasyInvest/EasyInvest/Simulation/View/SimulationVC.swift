//
//  SimulationVC.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 24/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

class SimulationVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var lblGrossAmount: UILabel!
    @IBOutlet weak var lblGrossAmountProfit: UILabel!

    @IBOutlet weak var lblInvestAmount: UILabel!
    @IBOutlet weak var lblGrossAmountDetail: UILabel!
    @IBOutlet weak var lblGrossAmountProfiltDetail: UILabel!
    @IBOutlet weak var lblTaxes: UILabel!
    @IBOutlet weak var lblNetAmount: UILabel!

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblMonthlyGrossRate: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblAnnualGrossRate: UILabel!
    @IBOutlet weak var lblRateProfit: UILabel!

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: - Properties
    var viewModel: SimulationViewModel!

    // MARK: Initializers
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(viewModel: SimulationViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        requestListeners()
        activityIndicator.startAnimating()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.viewModel.simulate()
    }

    // MARK: - Private Methods
    private func requestListeners() {

        self.viewModel.simulatorResultStatus.didChange = { [weak self] simulatorResult in

            guard let self = self else { return }

            DispatchQueue.main.async {

                switch simulatorResult {
                case .load(data: let data):

                    self.loadingView.isHidden = true

                    self.lblGrossAmount.text = "\(data.grossAmount.currency())"
                    self.lblGrossAmountProfit.text = "Rendimento total de \(data.grossAmountProfit.currency())"

                    self.lblInvestAmount.text = "\(data.investmentParameter!.investedAmount.currency())"
                    self.lblGrossAmountDetail.text = "\(data.grossAmount.currency())"
                    self.lblGrossAmountProfiltDetail.text = "\(data.grossAmountProfit.currency())"
                    self.lblTaxes.text = "\(data.taxesAmount.currency())(\(data.taxesRate)%)"
                    self.lblNetAmount.text = "\(data.netAmountProfit.currency())"

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    let muturityDate = dateFormatter.date(from: data.investmentParameter?.maturityDate ?? "")

                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    if let date = muturityDate {
                        self.lblDate.text = dateFormatter.string(from: date)
                    } else {
                        self.lblDate.text = ""
                    }

                    self.lblDays.text = "\(data.investmentParameter?.maturityTotalDays ?? 0)"
                    self.lblMonthlyGrossRate.text = "\(data.monthlyGrossRateProfit)%"
                    self.lblRate.text = "\(data.investmentParameter?.rate ?? 0)%"
                    self.lblAnnualGrossRate.text = "\(data.annualGrossRateProfit)%"
                    self.lblRateProfit.text = "\(data.rateProfit)%"

                case .loading:
                    self.loadingView.isHidden = false
                case .errored:
                    self.loadingView.isHidden = true

                    let alert = UIAlertController(title: "Opps!",
                                                  message: "Ocorreu um erro, tento novamente mais tarde",
                                                  preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)

                    self.lblGrossAmount.text = ""
                    self.lblGrossAmountProfit.text = "Rendimento total de)"
                    self.lblInvestAmount.text = ""
                    self.lblGrossAmountDetail.text = ""
                    self.lblGrossAmountProfiltDetail.text = ""
                    self.lblTaxes.text = ""
                    self.lblNetAmount.text = ""
                    self.lblDate.text = ""

                    self.lblDays.text = ""
                    self.lblMonthlyGrossRate.text = ""
                    self.lblRate.text = ""
                    self.lblAnnualGrossRate.text = ""
                    self.lblRateProfit.text = ""
                }
            }
        }
    }

    // MARK: - Action
    @IBAction func popScreen(_ sender: UIButton) {
        self.viewModel.popScreen()
    }

}
