//
//  SimulationViewModel.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 24/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

protocol SimulationViewModelDelegate: class {
    func popScreen()
}

class SimulationViewModel {

    // MARK: - Properrties
    var simulatorResultStatus = Observable(RequestStates<SimulatorResultModel>.loading)
    var simulatorResultModel: SimulatorResultModel
    var investModel: InvestModel

    weak var delegate: SimulationViewModelDelegate?

    // MARK: - Initializers
    init(with simulatorResultModel: SimulatorResultModel, investModel: InvestModel) {
        self.simulatorResultModel = simulatorResultModel
        self.investModel = investModel
    }

    // MARK: - Requests
    func simulate() {

        self.simulatorResultStatus.value = .loading

        guard let amount = self.investModel.investAmount,
            let rate = self.investModel.cdiPercent,
            let date = self.investModel.finalDate else { return }

        API().investServices.simulateInvest(amount: amount,
                                            rate: rate, date: date,
                                            success: { [weak self] (result) in
            guard let self = self else { return }
            self.simulatorResultModel = result
            self.simulatorResultStatus.value = .load(data: result)

        }, failure: { [weak self] (error) in

            guard let self = self else { return }
            self.simulatorResultStatus.value = .errored(error: error)
        })
    }

    func popScreen() {
        delegate?.popScreen()
    }
}
