//
//  InvestVC.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 21/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

class InvestVC: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtCdi: UITextField!

    @IBOutlet weak var btnSimulate: UIButton!

    // MARK: - Properties
    var viewModel: InvestViewModel!

    // MARK: Initializers
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    init(viewModel: InvestViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.valueTextFieldDidChange),
            name: UITextField.textDidChangeNotification,
            object: txtAmount)

        // Add tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.finishInsertValue(_:)))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
    }

    // MARK: - Methods
    @objc func finishInsertValue(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    // MARK: - Actions
    @IBAction func goToSimulate(_ sender: UIButton) {

        if self.viewModel.canGoSimulate() {
            self.viewModel.simulate()
        }
    }

    @IBAction func dateFieldEditing(_ sender: UITextField) {

        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.minimumDate = Date()

        sender.inputView = datePickerView

        datePickerView.addTarget(self,
                                 action: #selector(datePickerValueChanged),
                                 for: UIControl.Event.valueChanged)
    }

    // MARK: - DatePicker
    @objc func datePickerValueChanged(sender: UIDatePicker) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"

        txtDate.text = dateFormatter.string(from: sender.date)

        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.viewModel.investModel.finalDate = dateFormatter.string(from: sender.date)
    }

    @objc func valueTextFieldDidChange() {
        if let amount = self.txtAmount.text?.currencyInputFormatting() {
            self.txtAmount.text = amount
        }
    }
}

// MARK: - TextFieldDelegate
extension InvestVC: UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {

        guard let string = textField.text else { return }

        if textField == txtAmount {
            self.viewModel.investModel.investAmount = Double(string.cleanCurrencyFormat)
        } else if textField == txtCdi {
            self.viewModel.investModel.cdiPercent = Int(string)
        }

        if self.viewModel.canGoSimulate() {
            btnSimulate.backgroundColor = .blue
            btnSimulate.isEnabled = true
        }
    }
}
