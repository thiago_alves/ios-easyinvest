//
//  EasyCodable.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 23/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import Foundation

protocol EasyCodable: Codable {
    init?(_ dictionary: [String: Any])
    init?(_ data: Data)
    func dictionary() -> [String: Any]?
    func jsonString() -> String
}

extension EasyCodable {
    init?(_ dictionary: [String: Any]) {
        do {
            let data = try JSONSerialization.data(withJSONObject: dictionary, options: [])
            let object = try JSONDecoder().decode(Self.self, from: data)
            self = object
        } catch {
            return nil
        }
    }

    init?(_ data: Data) {
        do {
            let decoder = JSONDecoder()
            let object = try decoder.decode(Self.self, from: data)
            self = object
        } catch let error {
            #if DEVELOPMENT || QA
            print("\n❓JSONDecoder -> \(Self.self): \(error)\n")
            #endif
            return nil
        }
    }

    func dictionary() -> [String: Any]? {
        if let jsonData = try? JSONEncoder().encode(self),
            let dict = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any] {
            return dict
        }
        return nil
    }

    func jsonString() -> String {
        if  let data = try? JSONEncoder().encode(self),
            let str = String(data: data, encoding: .utf8) {
            return str
        }
        return "{}"
    }
}
