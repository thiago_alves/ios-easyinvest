//
//  InvestCoordinator.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 21/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

class InvestCoordinator {

    var simulationCoordinator: SimulationCoordinator?

    var navigationController: UINavigationController?

    var model: InvestModel?
    var viewModel: InvestViewModel?
    var view: InvestVC?

    func start() -> UINavigationController {

        model = InvestModel()
        viewModel = InvestViewModel(with: model!)
        viewModel?.delegate = self
        view = InvestVC(viewModel: viewModel!)

        let navigationController = UINavigationController(rootViewController: view!)
        navigationController.isNavigationBarHidden = true

        self.navigationController = navigationController

        return navigationController
    }
}

extension InvestCoordinator: InvestViewModelDelegate {
    func openSimulator(investModel: InvestModel) {
        simulationCoordinator = SimulationCoordinator()
        simulationCoordinator!.start(with: investModel, in: self.navigationController!)
    }
}
