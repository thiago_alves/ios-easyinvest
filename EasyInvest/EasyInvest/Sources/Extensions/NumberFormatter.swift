//
//  NumberFormatter.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 25/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

extension NumberFormatter {

    static var currencyFromatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = "R$"
        formatter.allowsFloats = true
        formatter.decimalSeparator = ","
        formatter.groupingSeparator = "."
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "pt_BR")

        return formatter
    }
}
