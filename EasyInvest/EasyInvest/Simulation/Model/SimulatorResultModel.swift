//
//  SimulatorResultModel.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 23/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

struct SimulatorResultModel: Codable, Equatable {

    var investmentParameter: InvestmentParameter?

    var grossAmount: Double = 0.0               // Valor bruto do investimento
    var taxesAmount: Double = 0.0               // Valor do IR
    var netAmount: Double = 0.0                 // Valor líquido
    var grossAmountProfit: Double = 0.0         // Rentabilidade bruta
    var netAmountProfit: Double = 0.0           // Rentabilidade líquida
    var annualGrossRateProfit: Double = 0.0     // Rentabilidade bruta anual
    var monthlyGrossRateProfit: Double = 0.0    // Rentabilidade bruta mensal
    var taxesRate: Double = 0.0                 // Faixa do IR (%)
    var rateProfit: Double = 0.0                // Rentabilidade no período

    struct InvestmentParameter: Codable, Equatable {
        var investedAmount: Double = 0.0        // O valor a ser investido
        var maturityTotalDays: Int = 0          // Dias corridos
        var maturityDate: String = ""           // Data de vencimento
        var rate: Int = 0                       // Percentual do papel
    }

    static func == (lhs: SimulatorResultModel, rhs: SimulatorResultModel) -> Bool {
        return lhs.investmentParameter == rhs.investmentParameter &&
        lhs.grossAmount == rhs.grossAmount &&
        lhs.taxesAmount == rhs.taxesAmount &&
        lhs.netAmount == rhs.netAmount &&
        lhs.grossAmountProfit == rhs.grossAmountProfit &&
        lhs.netAmountProfit == rhs.netAmountProfit &&
        lhs.annualGrossRateProfit == rhs.annualGrossRateProfit &&
        lhs.monthlyGrossRateProfit == rhs.monthlyGrossRateProfit &&
        lhs.taxesAmount == rhs.taxesAmount &&
        lhs.rateProfit == rhs.rateProfit
    }
}
