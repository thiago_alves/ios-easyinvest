//
//  AppCoordinator.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 21/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

class AppCoordinator {

    var window: UIWindow
    var investCoordinator: InvestCoordinator?

    required init(window: UIWindow) {
        self.window = window

        self.window.makeKeyAndVisible()
    }

    func start() {
        investCoordinator = InvestCoordinator()
        self.window.rootViewController = investCoordinator?.start()
    }
}
