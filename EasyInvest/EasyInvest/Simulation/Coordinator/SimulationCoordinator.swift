//
//  SimulationCoordinator.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 24/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

class SimulationCoordinator {

    var view: SimulationVC?
    var viewModel: SimulationViewModel?
    var navigation: UINavigationController?

    func start(with investModel: InvestModel, in navigationController: UINavigationController) {

        self.navigation = navigationController

        let model = SimulatorResultModel()
        viewModel = SimulationViewModel(with: model, investModel: investModel)
        viewModel!.delegate = self

        view = SimulationVC(viewModel: viewModel!)

        navigationController.pushViewController(view!, animated: true)
    }
}

extension SimulationCoordinator: SimulationViewModelDelegate {
    func popScreen() {
        self.navigation?.popViewController(animated: true)
    }
}
