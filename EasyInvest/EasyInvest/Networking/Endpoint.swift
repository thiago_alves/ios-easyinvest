//
//  Endpoint.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 22/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import Foundation

// //---------------- ENDPOINTS -----------------------
public class Endpoint {
    /// A tuple that recieves an URI and the http request method
    typealias EndpointType = (uri: String, method: String)

    /// Contains the http method String simplified for
    struct HTTPMethod {
        static let get    = "GET"
        static let post   = "POST"
        static let update = "PUT"
        static let delete = "DELETE"
        static let head   = "HEAD"
    }

    // Endpoints list
    struct Calculator {
        static let simulate: EndpointType = (uri: "/calculator/simulate", method: HTTPMethod.get)
    }

}
