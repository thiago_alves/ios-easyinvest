//
//  RequestStates.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 22/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import Foundation

enum RequestStates<T> {
    case loading
    case errored(error: Error)
    case load(data: T)
}
