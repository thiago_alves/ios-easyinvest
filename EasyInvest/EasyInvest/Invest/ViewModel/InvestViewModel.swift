//
//  SimulatorViewModel.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 24/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

protocol InvestViewModelDelegate: class {
    func openSimulator(investModel: InvestModel)
}

class InvestViewModel {

    // MARK: - Properties
    weak var delegate: InvestViewModelDelegate?
    var investModel: InvestModel

    // MARK: - Initializers
    init(with investModel: InvestModel) {
        self.investModel = investModel
    }

    // MARK: - Methods
    func canGoSimulate() -> Bool {

        if self.investModel.investAmount != nil, self.investModel.finalDate != nil, self.investModel.cdiPercent != nil {
            return true
        }
        return false
    }

    func simulate() {
        self.delegate?.openSimulator(investModel: self.investModel)
    }
}
