//
//  InvestUITest.swift
//  EasyInvestUITests
//
//  Created by Thiago Felipe Alves on 28/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import XCTest

class InvestUITest: XCTestCase {

    var app: XCUIApplication!

    override func setUp() {
        super.setUp()

        continueAfterFailure = false

        app = XCUIApplication()
        app.launch()
    }

    func testInsertValues() {

        let elementsQuery = app.scrollViews.otherElements
        elementsQuery.textFields["R$"].tap()
        elementsQuery.textFields["R$"].typeText("100000")

        let diaMSAnoTextField = elementsQuery.textFields["dia/mês/ano"]
        diaMSAnoTextField.tap()

        app.datePickers.pickerWheels["2018"].adjust(toPickerWheelValue: "2025")

        let cdiText = app.scrollViews.otherElements.textFields["100%"]
        cdiText.tap()
        cdiText.typeText("100")

        // close keyboard
        app.children(matching: .window).element(boundBy: 0).children(matching: .other)
            .element.children(matching: .other).element.children(matching: .other)
            .element.children(matching: .other).element.children(matching: .scrollView).element.tap()

        XCTAssertTrue(app.buttons.element(boundBy: 0).isEnabled)
    }

    func testSimulationScreen() {

        testInsertValues()

        app.buttons.element(boundBy: 0).tap()

        let scrollViewsQuery = XCUIApplication().scrollViews
        let label = scrollViewsQuery.otherElements.staticTexts["Resultado da simulação"]

        XCTAssert(label.exists)
    }
}
