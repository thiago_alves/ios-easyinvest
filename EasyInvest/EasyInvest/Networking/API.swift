//
//  API.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 22/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import Foundation

//---------------- API -----------------------
class API {
    enum Environment: String {
///calculator/simulate
        case production = "https://api-simulator-calc.easynvest.com.br"

        //Just to be more readable
        func getValue() -> String {
            return self.rawValue
        }
    }

    let environment: Environment
    init() {
        environment = Environment.production
    }

    lazy var investServices = InvestAPI()
}
