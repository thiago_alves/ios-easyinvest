//
//  InvestAPI.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 23/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

class InvestAPI: Requester {

    func simulateInvest(amount: Double,
                        rate: Int,
                        date: String,
                        success: CompletionWithSuccess<SimulatorResultModel>?,
                        failure: CompletionWithFailure?) {

        let body = ["investedAmount": amount,
                    "index": "CDI",
                    "rate": rate,
                    "isTaxFree": false,
                    "maturityDate": date] as [String: Any]

        let url = urlComposer(using: Endpoint.Calculator.simulate)
        let request = requestComposer(using: url, headers: [:], body: body)

        dataTask(using: request) { [weak self] (data, error) in
            if error == nil {
                guard let dataObject = data else {
                    return
                }
                let simulator = self?.JSONDecode(to: SimulatorResultModel.self, from: dataObject)
                guard let simulatorResult = simulator else {
                    return
                }
                success?(simulatorResult)
            } else {
                failure?(error!)
            }
        }
    }
}
