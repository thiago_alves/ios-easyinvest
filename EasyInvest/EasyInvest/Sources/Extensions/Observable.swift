//
//  Observable.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 24/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

class Observable<T> {
    fileprivate var _value: T?
    var didChange: ((T) -> Void)?
    var value: T {
        set {
            _value = newValue
            didChange?(_value!)
        }
        get {
            return _value!
        }
    }

    init(_ value: T) {
        self.value = value
    }

    deinit {
        _value = nil
        didChange = nil
    }
}
