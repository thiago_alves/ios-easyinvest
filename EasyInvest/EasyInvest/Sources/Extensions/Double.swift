//
//  Double.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 29/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

extension Double {

    func currency() -> String {

        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "pt-BR")
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
            return formattedTipAmount
        }

        return ""
    }
}
