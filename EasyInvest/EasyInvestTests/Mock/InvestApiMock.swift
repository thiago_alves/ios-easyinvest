//
//  InvestApiMock.swift
//  EasyInvestTests
//
//  Created by Thiago Felipe Alves on 31/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import XCTest

@testable import EasyInvest

class InvestApiMock: InvestAPI {

    override func simulateInvest(amount: Double,
                                 rate: Int,
                                 date: String,
                                 success: ((SimulatorResultModel) -> ())?, failure: CompletionWithFailure?) {

        let data: Data = """
        {"investmentParameter": {
            "investedAmount": 1000,
            "yearlyInterestRate": 7.2896,
            "maturityTotalDays": 364,
            "maturityBusinessDays": 254,
            "maturityDate": "2019-10-28T00:00:00",
            "rate": 100,
            "isTaxFree": false
        },
        "grossAmount": 1073.5,
        "taxesAmount": 12.86,
        "netAmount": 1060.64,
        "grossAmountProfit": 73.5,
        "netAmountProfit": 60.64,
        "annualGrossRateProfit": 7.35,
        "monthlyGrossRateProfit": 0.59,
        "dailyGrossRateProfit": 0.000279251421676285,
        "taxesRate": 17.5,
        "rateProfit": 7.2896,
        "annualNetRateProfit": 6.06 }
        """.data(using: .utf8)!

        let simulateInfoDecode = self.JSONDecode(to: SimulatorResultModel.self, from: data)

        guard let simulation = simulateInfoDecode else {
            return
        }

        success?(simulation)
    }

}
