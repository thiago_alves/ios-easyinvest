//
//  InvestModel.swift
//  EasyInvest
//
//  Created by Thiago Felipe Alves on 24/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import UIKit

struct InvestModel {

    var investAmount: Double?
    var finalDate: String?
    var cdiPercent: Int?
}
