//
//  InvestModelFactory.swift
//  EasyInvestTests
//
//  Created by Thiago Felipe Alves on 31/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import XCTest

@testable import EasyInvest

class InvestModelFactory {

    static func generate() -> InvestModel {

        return InvestModel(investAmount: 1000.00, finalDate: "2019-10-28", cdiPercent: 100)
    }

}
