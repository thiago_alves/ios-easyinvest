//
//  SimulatorResultModelFactory.swift
//  EasyInvestTests
//
//  Created by Thiago Felipe Alves on 31/10/18.
//  Copyright © 2018 Thiago Felipe Alves. All rights reserved.
//

import XCTest

@testable import EasyInvest

class SimulatorResultModelFactory {

    static func generate () -> SimulatorResultModel {

        let investParameters = SimulatorResultModel.InvestmentParameter(investedAmount: 1000,
                                                                        maturityTotalDays: 364,
                                                                        maturityDate: "2019-10-28T00:00:00",
                                                                        rate: 100)
        let simulatorResultModel = SimulatorResultModel(investmentParameter: investParameters,
                                                        grossAmount: 1073.5,
                                                        taxesAmount: 12.86,
                                                        netAmount: 1060.64,
                                                        grossAmountProfit: 73.5,
                                                        netAmountProfit: 60.64,
                                                        annualGrossRateProfit: 7.35,
                                                        monthlyGrossRateProfit: 0.59,
                                                        taxesRate: 17.5,
                                                        rateProfit: 7.2896)

        return simulatorResultModel
    }

}
